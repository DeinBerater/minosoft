# How to contribute

First, thank you for visiting this page and for even thinking about contributing.

## Things you can do

### Issues
If you find a bug (in master **AND** in development), feel free to follow these steps:
1. Check if this bug is a bug and can be recreated.
2. Check if this bug is already reported (use the issue search or google).
3. Open the issue and describe the problem as much as needed (not as you can!)
4. Include the version of minosoft (or commit name), your OS and all information like that
5. Include a log. If you know what you are doing, include the interesting paths (and the **first** exception that happened).
If you don't know much about this shit, just include the whole log (You may need to censure email addresses, ...). Paste the log into a code block and do not use other 3rd party sites!
6. Describe the bug and follow the rules

### Development
Please take a look at [/doc/contributing/Development.md](doc/contributing/Development.md)

### Share
Minosoft is an open source project, it helps if you recommend it and share it more users (probably) use it. I'll feel good, that I developed something useful, and people start contributing.

### Donating

I started this project in mind, that I'll never earn money from it. Just doing it for fun, so I am not dependent on it. So: Currently not available and not planned.

## Issue and MR rules
- Do not spam, we will answer when we have time.
- Use english whenever possible, we would even allow using german, but try to use english.
- Don't hesitate against other developers.

## Contact us

If you want to ask me, the team or whoever in this project a question, you can do the following things:
 - IRC. I am very often in the channel `#mcdevs` on [freenode](https://freenode.net/). You can contact me there, my nickname is - as always - @Bixilon.
 - Issues: Just open an issue if you want to ask it, or suggest something. If it is a really short thing, this is not recommended, but even if, it is no problem.
 - E-mail: You can contact me here: [bixilon@bixilon.de](mailto:bixilon@bixilon.de)
 - TeamSpeak: You can join my teamspeak (bixilon.de) and poke me there.

