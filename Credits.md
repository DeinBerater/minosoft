# Credits
First of all: Thanks to the authors from [wiki.vg](https://wiki.vg) for documenting the protocol.
Also, special thanks to [Pokechu22](https://github.com/Pokechu22) for being available on their irc and for helping with some problems.

## Thanks to the following projects:
 - [Burger](https://github.com/Pokechu22/Burger)
 - [wiki.vg](https://wiki.vg)
 - [Glowstone](https://glowstone.net/) For letting me see how you did, when something was documented pretty badly
 - [Mojang](https://mojang.com) For including data generators in 1.13 (after flattening update)

## Thanks to all contributors:
 - [Bixilon](https://bixilon.de) (Wow, that's me :D)
 - [Lukas](https://gitlab.bixilon.de/lukas) For giving me motivation and developing the rendering (Game graphics)
 - [Alex](https://gitlab.bixilon.de/alexamg) For the awesome logo