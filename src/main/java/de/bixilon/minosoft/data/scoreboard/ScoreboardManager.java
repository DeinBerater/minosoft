/*
 * Minosoft
 * Copyright (C) 2020 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.minosoft.data.scoreboard;

import java.util.HashMap;

public class ScoreboardManager {
    private final HashMap<String, Team> teams = new HashMap<>();
    private final HashMap<String, ScoreboardObjective> objectives = new HashMap<>();

    public void addTeam(Team team) {
        this.teams.put(team.getName(), team);
    }

    public Team getTeam(String name) {
        return this.teams.get(name);
    }

    public void removeTeam(String name) {
        this.teams.remove(name);
    }

    public void addObjective(ScoreboardObjective objective) {
        this.objectives.put(objective.getObjectiveName(), objective);
    }

    public void removeObjective(String name) {
        this.objectives.remove(name);
    }

    public ScoreboardObjective getObjective(String objective) {
        return this.objectives.get(objective);
    }
}
