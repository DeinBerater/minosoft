/*
 * Minosoft
 * Copyright (C) 2020 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.minosoft.protocol.packets.serverbound.play;

import de.bixilon.minosoft.data.Difficulties;
import de.bixilon.minosoft.data.player.Hands;
import de.bixilon.minosoft.logging.Log;
import de.bixilon.minosoft.protocol.network.Connection;
import de.bixilon.minosoft.protocol.packets.ServerboundPacket;
import de.bixilon.minosoft.protocol.protocol.OutPacketBuffer;
import de.bixilon.minosoft.protocol.protocol.Packets;

import static de.bixilon.minosoft.protocol.protocol.ProtocolVersions.V_14W03B;
import static de.bixilon.minosoft.protocol.protocol.ProtocolVersions.V_15W31A;

public class PacketClientSettings implements ServerboundPacket {

    public final String locale;
    public final byte renderDistance;

    public Hands mainHand;

    public PacketClientSettings(String locale, int renderDistance) {
        this.locale = locale;
        this.renderDistance = (byte) renderDistance;
    }

    public PacketClientSettings(String locale, int renderDistance, Hands mainHand) {
        this.locale = locale;
        this.renderDistance = (byte) renderDistance;
        this.mainHand = mainHand;
    }

    @Override
    public OutPacketBuffer write(Connection connection) {
        OutPacketBuffer buffer = new OutPacketBuffer(connection, Packets.Serverbound.PLAY_CLIENT_SETTINGS);
        buffer.writeString(this.locale); // locale
        buffer.writeByte(this.renderDistance); // render Distance
        buffer.writeByte((byte) 0x00); // chat settings (nobody uses them)
        buffer.writeBoolean(true); // chat colors
        if (buffer.getVersionId() < V_14W03B) {
            buffer.writeByte((byte) Difficulties.NORMAL.ordinal()); // difficulty
            buffer.writeBoolean(true); // cape
        } else {
            buffer.writeByte((byte) 0b01111111); // ToDo: skin parts
        }
        if (buffer.getVersionId() >= V_15W31A) {
            buffer.writeVarInt(this.mainHand.ordinal());
        }
        return buffer;
    }

    @Override
    public void log() {
        Log.protocol(String.format("[OUT] Sending settings (locale=%s, renderDistance=%d)", this.locale, this.renderDistance));
    }
}
